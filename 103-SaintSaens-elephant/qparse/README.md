# 103 Saint-Saens elephant

### Parsing with a beat file:
```
../monoparse -v 5 -a ../grammars/schema-38.wta -m perf/103-bandlab0.mid -config ../params.ini  -beats qparse/103-bandlab0_beats.txt -ts 3/8 -clef F4 -max -staccato -o qparse/103-bandlab0.mei
```
The beat file `qparse/103-bandlab0_beats.txt`
was produced with ... from ...

### Parsing without a beat file,
alternatives to option `-beats`  
- `-barsec 0.9782595`  nb of seconds per bar  
  cf. 
  - `103-bandlab0-097.mei`  
  - `103-bandlab1-097.mei`  
  - `103-bandlab2-097.mei`  
  - `103-bandlab3-097.mei`  
 - `103-bandlab4-097.mei`  

- `-tempo 184` 
   where 184 bpm ~ (3 * 60) / 0.9782595



 ../parse.sh 103-bandlab0 3 8 92



