<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE score-partwise PUBLIC "-//Recordare//DTD MusicXML 4.0 Partwise//EN" "http://www.musicxml.org/dtds/partwise.dtd">
<score-partwise version="4.0">
  <movement-title>Sonatine N.2 op.60</movement-title>
  <identification>
    <creator type="composer">D. F. Kuhlau</creator>
    <encoding>
      <software>MuseScore 4.1.1</software>
      <encoding-date>2023-11-01</encoding-date>
      <supports element="accidental" type="yes"/>
      <supports element="beam" type="yes"/>
      <supports element="print" attribute="new-page" type="yes" value="yes"/>
      <supports element="print" attribute="new-system" type="yes" value="yes"/>
      <supports element="stem" type="yes"/>
      </encoding>
    </identification>
  <defaults>
    <scaling>
      <millimeters>6.99911</millimeters>
      <tenths>40</tenths>
      </scaling>
    <page-layout>
      <page-height>1596.77</page-height>
      <page-width>1233.87</page-width>
      <page-margins type="even">
        <left-margin>85.7252</left-margin>
        <right-margin>85.7252</right-margin>
        <top-margin>85.7252</top-margin>
        <bottom-margin>85.7252</bottom-margin>
        </page-margins>
      <page-margins type="odd">
        <left-margin>85.7252</left-margin>
        <right-margin>85.7252</right-margin>
        <top-margin>85.7252</top-margin>
        <bottom-margin>85.7252</bottom-margin>
        </page-margins>
      </page-layout>
    <word-font font-family="FreeSerif" font-size="9.6"/>
    <lyric-font font-family="Edwin" font-size="10"/>
    </defaults>
  <credit page="1">
    <credit-type>title</credit-type>
    <credit-words default-x="616.935" default-y="1500.05" justify="center" valign="top" font-family="Times New Roman" font-size="23">Sonatine N.2 op.60</credit-words>
    </credit>
  <credit page="1">
    <credit-type>composer</credit-type>
    <credit-words default-x="1148.15" default-y="1433.05" justify="right" valign="top" font-family="Times New Roman" font-size="11.5">D. F. Kuhlau</credit-words>
    </credit>
  <part-list>
    <score-part id="P1">
      <part-name>Voice</part-name>
      <score-instrument id="P1-I1">
        <instrument-name>SmartMusic SoftSynth 1</instrument-name>
        <instrument-sound>keyboard.piano.grand</instrument-sound>
        </score-instrument>
      <midi-device id="P1-I1" port="1"></midi-device>
      <midi-instrument id="P1-I1">
        <midi-channel>1</midi-channel>
        <midi-program>1</midi-program>
        <volume>77.9528</volume>
        <pan>0</pan>
        </midi-instrument>
      </score-part>
    </part-list>
  <part id="P1">
    <measure number="1" width="295.37">
      <print>
        <system-layout>
          <system-margins>
            <left-margin>50.00</left-margin>
            <right-margin>0.00</right-margin>
            </system-margins>
          <top-system-distance>170.00</top-system-distance>
          </system-layout>
        </print>
      <attributes>
        <divisions>8</divisions>
        <key>
          <fifths>3</fifths>
          <mode>major</mode>
          </key>
        <time symbol="common">
          <beats>4</beats>
          <beat-type>4</beat-type>
          </time>
        <clef>
          <sign>F</sign>
          <line>4</line>
          </clef>
        </attributes>
      <direction placement="below">
        <direction-type>
          <dynamics default-x="2.32" default-y="-40.00" relative-y="-25.00">
            <f/>
            </dynamics>
          </direction-type>
        <sound dynamics="100.00"/>
        </direction>
      <direction placement="above">
        <direction-type>
          <words default-y="29.40" relative-y="10.00" font-weight="bold" font-family="Times New Roman" font-size="13.4">Allegro con spirito</words>
          </direction-type>
        </direction>
      <note default-x="117.76" default-y="0.00">
        <pitch>
          <step>A</step>
          <octave>3</octave>
          </pitch>
        <duration>16</duration>
        <voice>1</voice>
        <type>half</type>
        <stem>down</stem>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      <note default-x="184.04" default-y="-15.00">
        <pitch>
          <step>E</step>
          <octave>3</octave>
          </pitch>
        <duration>8</duration>
        <voice>1</voice>
        <type>quarter</type>
        <stem>down</stem>
        <notations>
          <slur type="stop" number="1"/>
          </notations>
        </note>
      <note default-x="228.23" default-y="-20.00">
        <rest/>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        </note>
      <note default-x="265.57" default-y="10.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      </measure>
    <measure number="2" width="215.14">
      <note default-x="13.00" default-y="10.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>16</duration>
        <voice>1</voice>
        <type>half</type>
        <stem>down</stem>
        </note>
      <note default-x="79.28" default-y="0.00">
        <pitch>
          <step>A</step>
          <octave>3</octave>
          </pitch>
        <duration>8</duration>
        <voice>1</voice>
        <type>quarter</type>
        <stem>down</stem>
        <notations>
          <slur type="stop" number="1"/>
          </notations>
        </note>
      <note default-x="123.47" default-y="-20.00">
        <rest/>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        </note>
      <attributes>
        <clef>
          <sign>G</sign>
          <line>2</line>
          </clef>
        </attributes>
      <note default-x="180.18" default-y="-35.00">
        <pitch>
          <step>F</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>up</stem>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      </measure>
    <measure number="3" width="140.31">
      <direction placement="below">
        <direction-type>
          <wedge type="diminuendo" number="1" default-y="-60.00"/>
          </direction-type>
        </direction>
      <note default-x="13.00" default-y="-35.00">
        <pitch>
          <step>F</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>28</duration>
        <voice>1</voice>
        <type>half</type>
        <dot/>
        <dot/>
        <stem>up</stem>
        </note>
      <direction placement="below">
        <direction-type>
          <wedge type="stop" number="1"/>
          </direction-type>
        </direction>
      <note default-x="104.95" default-y="-40.00">
        <pitch>
          <step>E</step>
          <octave>4</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>up</stem>
        </note>
      </measure>
    <measure number="4" width="225.39">
      <direction placement="below">
        <direction-type>
          <dynamics default-x="3.10" default-y="-48.90" relative-y="-25.00">
            <p/>
            </dynamics>
          </direction-type>
        <sound dynamics="55.56"/>
        </direction>
      <note default-x="18.76" default-y="-45.00">
        <pitch>
          <step>D</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>12</duration>
        <voice>1</voice>
        <type>quarter</type>
        <dot/>
        <accidental>sharp</accidental>
        <stem>up</stem>
        </note>
      <note default-x="74.78" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        </note>
      <note default-x="94.42" default-y="-20.00">
        <pitch>
          <step>B</step>
          <octave>4</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        </note>
      <note default-x="114.06" default-y="-40.00">
        <pitch>
          <step>E</step>
          <octave>4</octave>
          </pitch>
        <duration>8</duration>
        <voice>1</voice>
        <type>quarter</type>
        <stem>up</stem>
        <notations>
          <slur type="stop" number="1"/>
          </notations>
        </note>
      <note default-x="158.24" default-y="-20.00">
        <rest/>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        </note>
      <note default-x="195.59" default-y="-10.00">
        <pitch>
          <step>D</step>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <accidental>natural</accidental>
        <stem>down</stem>
        </note>
      </measure>
    <measure number="5" width="136.21">
      <direction placement="below">
        <direction-type>
          <wedge type="diminuendo" number="1" default-y="-60.00"/>
          </direction-type>
        </direction>
      <note default-x="13.00" default-y="-10.00">
        <pitch>
          <step>D</step>
          <octave>5</octave>
          </pitch>
        <duration>28</duration>
        <voice>1</voice>
        <type>half</type>
        <dot/>
        <dot/>
        <stem>down</stem>
        </note>
      <direction placement="below">
        <direction-type>
          <wedge type="stop" number="1"/>
          </direction-type>
        </direction>
      <note default-x="104.95" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        </note>
      </measure>
    <measure number="6" width="324.99">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0.00</left-margin>
            <right-margin>0.00</right-margin>
            </system-margins>
          <system-distance>190.40</system-distance>
          </system-layout>
        </print>
      <direction placement="below">
        <direction-type>
          <dynamics default-x="3.10" default-y="-40.00" relative-y="-25.00">
            <p/>
            </dynamics>
          </direction-type>
        <sound dynamics="55.56"/>
        </direction>
      <note default-x="99.77" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>12</duration>
        <voice>1</voice>
        <type>quarter</type>
        <dot/>
        <stem>down</stem>
        <notations>
          <articulations>
            <accent placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="154.33" default-y="-20.00">
        <pitch>
          <step>B</step>
          <octave>4</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        </note>
      <note default-x="171.83" default-y="-25.00">
        <grace/>
        <pitch>
          <step>A</step>
          <octave>4</octave>
          </pitch>
        <voice>1</voice>
        <type>32nd</type>
        <stem>up</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        <beam number="3">begin</beam>
        </note>
      <note default-x="184.43" default-y="-15.00">
        <grace/>
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <voice>1</voice>
        <type>32nd</type>
        <stem>up</stem>
        <beam number="1">continue</beam>
        <beam number="2">continue</beam>
        <beam number="3">continue</beam>
        </note>
      <note default-x="197.03" default-y="-20.00">
        <grace/>
        <pitch>
          <step>B</step>
          <octave>4</octave>
          </pitch>
        <voice>1</voice>
        <type>32nd</type>
        <stem>up</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        <beam number="3">end</beam>
        </note>
      <note default-x="210.62" default-y="-25.00">
        <pitch>
          <step>A</step>
          <octave>4</octave>
          </pitch>
        <duration>8</duration>
        <voice>1</voice>
        <type>quarter</type>
        <stem>up</stem>
        </note>
      <note default-x="253.66" default-y="-20.00">
        <rest/>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        </note>
      <note default-x="290.03" default-y="-25.00">
        <pitch>
          <step>A</step>
          <octave>4</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>up</stem>
        </note>
      </measure>
    <measure number="7" width="244.34">
      <direction placement="below">
        <direction-type>
          <dynamics default-x="3.10" default-y="-40.00" relative-y="-25.00">
            <p/>
            </dynamics>
          </direction-type>
        <sound dynamics="55.56"/>
        </direction>
      <note default-x="13.00" default-y="-30.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>up</stem>
        </note>
      <note default-x="41.69" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      <note default-x="70.38" default-y="-10.00">
        <pitch>
          <step>D</step>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        </note>
      <note default-x="99.08" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      <note default-x="127.77" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        </note>
      <note default-x="156.46" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      <note default-x="185.15" default-y="10.00">
        <pitch>
          <step>A</step>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        </note>
      <note default-x="213.85" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      </measure>
    <measure number="8" width="247.42">
      <direction placement="below">
        <direction-type>
          <dynamics default-x="3.10" default-y="-40.00" relative-y="-25.00">
            <p/>
            </dynamics>
          </direction-type>
        <sound dynamics="55.56"/>
        </direction>
      <note default-x="13.00" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        </note>
      <note default-x="41.69" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      <note default-x="70.38" default-y="30.00">
        <pitch>
          <step>E</step>
          <octave>6</octave>
          </pitch>
        <duration>16</duration>
        <voice>1</voice>
        <type>half</type>
        <stem>down</stem>
        </note>
      <note default-x="134.94" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      <note default-x="163.63" default-y="-10.00">
        <pitch>
          <step>D</step>
          <octave>5</octave>
          </pitch>
        <duration>1</duration>
        <voice>1</voice>
        <type>32nd</type>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        <beam number="3">begin</beam>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      <note default-x="181.63" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>1</duration>
        <voice>1</voice>
        <type>32nd</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <beam number="2">continue</beam>
        <beam number="3">continue</beam>
        </note>
      <note default-x="199.63" default-y="-20.00">
        <pitch>
          <step>B</step>
          <octave>4</octave>
          </pitch>
        <duration>1</duration>
        <voice>1</voice>
        <type>32nd</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <beam number="2">continue</beam>
        <beam number="3">continue</beam>
        </note>
      <note default-x="217.62" default-y="-25.00">
        <pitch>
          <step>A</step>
          <octave>4</octave>
          </pitch>
        <duration>1</duration>
        <voice>1</voice>
        <type>32nd</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        <beam number="3">end</beam>
        </note>
      </measure>
    <measure number="9" width="245.67">
      <direction placement="below">
        <direction-type>
          <dynamics default-x="3.10" default-y="-50.50" relative-y="-25.00">
            <p/>
            </dynamics>
          </direction-type>
        <sound dynamics="55.56"/>
        </direction>
      <note default-x="13.00" default-y="-30.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <notations>
          <slur type="stop" number="1"/>
          </notations>
        </note>
      <note default-x="49.37" default-y="-30.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <beam number="2">forward hook</beam>
        </note>
      <note default-x="68.50" default-y="-10.00">
        <pitch>
          <step>D</step>
          <octave>5</octave>
          </pitch>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        <stem>down</stem>
        <beam number="1">continue</beam>
        </note>
      <note default-x="104.87" default-y="-10.00">
        <pitch>
          <step>D</step>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">backward hook</beam>
        </note>
      <note default-x="124.00" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        <stem>down</stem>
        <beam number="1">begin</beam>
        </note>
      <note default-x="160.37" default-y="-15.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <beam number="2">forward hook</beam>
        </note>
      <note default-x="179.50" default-y="10.00">
        <pitch>
          <step>A</step>
          <octave>5</octave>
          </pitch>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        <stem>down</stem>
        <beam number="1">continue</beam>
        </note>
      <note default-x="215.87" default-y="10.00">
        <pitch>
          <step>A</step>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">backward hook</beam>
        </note>
      </measure>
    <measure number="10" width="300.92">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0.00</left-margin>
            <right-margin>0.00</right-margin>
            </system-margins>
          <system-distance>190.40</system-distance>
          </system-layout>
        </print>
      <note default-x="99.77" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <notations>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="130.83" default-y="-20.00">
        <rest/>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        </note>
      <direction placement="below">
        <direction-type>
          <dynamics default-x="2.32" default-y="-40.00" relative-y="-25.00">
            <f/>
            </dynamics>
          </direction-type>
        <sound dynamics="100.00"/>
        </direction>
      <note default-x="161.88" default-y="30.00">
        <pitch>
          <step>E</step>
          <octave>6</octave>
          </pitch>
        <duration>16</duration>
        <voice>1</voice>
        <type>half</type>
        <stem>down</stem>
        </note>
      <note default-x="231.75" default-y="-20.00">
        <rest/>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        </note>
      <note default-x="271.12" default-y="30.00">
        <pitch>
          <step>E</step>
          <octave>6</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      </measure>
    <measure number="11" width="239.44">
      <direction placement="below">
        <direction-type>
          <dynamics default-x="3.10" default-y="-40.00" relative-y="-25.00">
            <p/>
            </dynamics>
          </direction-type>
        <sound dynamics="55.56"/>
        </direction>
      <note default-x="13.00" default-y="30.00">
        <pitch>
          <step>E</step>
          <octave>6</octave>
          </pitch>
        <duration>12</duration>
        <voice>1</voice>
        <type>quarter</type>
        <dot/>
        <stem>down</stem>
        <notations>
          <articulations>
            <tenuto placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="72.05" default-y="25.00">
        <pitch>
          <step>D</step>
          <alter>1</alter>
          <octave>6</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <accidental>sharp</accidental>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        </note>
      <note default-x="94.15" default-y="20.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>6</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        </note>
      <note default-x="116.24" default-y="25.00">
        <pitch>
          <step>D</step>
          <alter>1</alter>
          <octave>6</octave>
          </pitch>
        <duration>12</duration>
        <voice>1</voice>
        <type>quarter</type>
        <dot/>
        <stem>down</stem>
        <notations>
          <articulations>
            <tenuto placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="175.29" default-y="20.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>6</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        </note>
      <note default-x="209.65" default-y="15.00">
        <pitch>
          <step>B</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <accidental>sharp</accidental>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        </note>
      </measure>
    <measure number="12" width="230.59">
      <note default-x="13.00" default-y="20.00">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>6</octave>
          </pitch>
        <duration>12</duration>
        <voice>1</voice>
        <type>quarter</type>
        <dot/>
        <stem>down</stem>
        <notations>
          <articulations>
            <tenuto placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="72.05" default-y="15.00">
        <pitch>
          <step>B</step>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <accidental>natural</accidental>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        </note>
      <note default-x="94.15" default-y="10.00">
        <pitch>
          <step>A</step>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        </note>
      <note default-x="114.85" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>8</duration>
        <voice>1</voice>
        <type>quarter</type>
        <stem>down</stem>
        <notations>
          <slur type="stop" number="1"/>
          </notations>
        </note>
      <note default-x="161.43" default-y="-20.00">
        <rest/>
        <duration>6</duration>
        <voice>1</voice>
        <type>eighth</type>
        <dot/>
        </note>
      <note default-x="200.79" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      </measure>
    <measure number="13" width="291.47">
      <note default-x="12.01" default-y="15.00">
        <grace slash="yes"/>
        <pitch>
          <step>B</step>
          <octave>5</octave>
          </pitch>
        <voice>1</voice>
        <type>eighth</type>
        <stem>up</stem>
        <notations>
          <slur type="start" placement="below" number="2"/>
          </notations>
        </note>
      <note default-x="32.75" default-y="10.00">
        <pitch>
          <step>A</step>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <notations>
          <slur type="stop" number="2"/>
          <articulations>
            <accent placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="63.80" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <notations>
          <slur type="stop" number="1"/>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="94.86" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <notations>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="125.91" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <notations>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="144.72" default-y="15.00">
        <grace slash="yes"/>
        <pitch>
          <step>B</step>
          <octave>5</octave>
          </pitch>
        <voice>1</voice>
        <type>eighth</type>
        <stem>up</stem>
        <notations>
          <slur type="start" placement="below" number="1"/>
          </notations>
        </note>
      <note default-x="165.46" default-y="10.00">
        <pitch>
          <step>A</step>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <notations>
          <slur type="stop" number="1"/>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      <direction placement="below">
        <direction-type>
          <wedge type="crescendo" number="1" default-y="-60.00"/>
          </direction-type>
        </direction>
      <note default-x="196.51" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <notations>
          <slur type="stop" number="1"/>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      <note default-x="227.56" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">continue</beam>
        <notations>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      <direction placement="below">
        <direction-type>
          <wedge type="stop" number="1"/>
          </direction-type>
        </direction>
      <note default-x="258.62" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>4</duration>
        <voice>1</voice>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <notations>
          <articulations>
            <staccato placement="above"/>
            </articulations>
          </notations>
        </note>
      </measure>
    <measure number="14" width="246.26">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0.00</left-margin>
            <right-margin>816.17</right-margin>
            </system-margins>
          <system-distance>190.40</system-distance>
          </system-layout>
        </print>
      <direction placement="below">
        <direction-type>
          <wedge type="diminuendo" number="1" default-y="-60.00"/>
          </direction-type>
        </direction>
      <note default-x="99.77" default-y="5.00">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>12</duration>
        <voice>1</voice>
        <type>quarter</type>
        <dot/>
        <stem>down</stem>
        <notations>
          <slur type="start" placement="above" number="1"/>
          </notations>
        </note>
      <note default-x="153.96" default-y="0.00">
        <pitch>
          <step>F</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
        </note>
      <note default-x="172.96" default-y="-5.00">
        <pitch>
          <step>E</step>
          <octave>5</octave>
          </pitch>
        <duration>2</duration>
        <voice>1</voice>
        <type>16th</type>
        <stem>down</stem>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
        </note>
      <direction placement="below">
        <direction-type>
          <wedge type="stop" number="1"/>
          </direction-type>
        </direction>
      <note default-x="201.71" default-y="-10.00">
        <pitch>
          <step>D</step>
          <alter>1</alter>
          <octave>5</octave>
          </pitch>
        <duration>8</duration>
        <voice>1</voice>
        <type>quarter</type>
        <accidental>sharp</accidental>
        <stem>down</stem>
        <notations>
          <slur type="stop" number="1"/>
          </notations>
        </note>
      </measure>
    </part>
  </score-partwise>
