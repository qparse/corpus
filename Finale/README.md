# Finale Files 
file in Finale format or edited with Finale


Directories :

* self: 
scores obtained by direct import of MIDI performances in Finale.

* qtf: 
scores obtained by 
    * quantization with qparse
    * export of quantization to MIDI file
    * process of this MIDI file with Finale.

* ref: 
original scores edited manually with Finale.
