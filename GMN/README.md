original scores in [Guido Music Notation format](http://guidolib.sourceforge.net/GUIDO/).

to render Guido GMN files online:

* [http://www.noteserver.org/noteserver.html](http://www.noteserver.org/noteserver.html)
* [http://guidolib.sourceforge.net/jsengine/index.html](http://guidolib.sourceforge.net/jsengine/index.html)

MIDI files obtained with [guido2midi](https://sourceforge.net/projects/guidolib/files/Qt%20Applications/).


