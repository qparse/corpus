# Musescore Files 
file in Musescore format or edited with Musescore


Directories :

* self: 
scores obtained by direct import of MIDI performances in Musescore.

* qtf: 
scores obtained by 
    * quantization with qparse
    * export of quantization to MIDI file
    * process of this MIDI file with Musescore.

* ref: 
original scores edited manually with Musescore.
