

scores of Polonaise for MuseScore prepared by Masahiko.

The one is quantized by squant and edited on grace notes,
and the other is the original score obtained by editing the former.