
# Lamarque-Goudard dataset

Dataset for MIDI2score monophonic transcription.
It contains monophonic extracts from the textbook:

> D’un Rythme à l’autre
> Elisabeth Lamarque, Marie-José Goudard
> ed. Henry Lemoine

Every directory contains one entry (i.e. one extract) of the dataset, with a name of the form
```
VXX-ComposerName-Piece 
```
where `VXX` is the number of the extract in the method,

-  `V` is the volume number, 1 to 4,
- `XX` is the number of the extract in the volume `V`.

The order of these numbers reflects the rhythmic complexity of the extract.

The subdirectories can be one of the following:

- `ref/` : reference score in one or several of the formats:
  - [MusicXML](https://www.musicxml.com)  score files : 
    - `.xml` or `.musx` or `.mxl` when produced with Finale
    - `.musicxml` (uncompressed) when exported from MuseScore or Finale
  - [MEI](https://music-encoding.org)  score file ( `.mei`)
  - [MuseScore](https://musescore.com) file ( `.mscz`)
  - [Dorico](https://new.steinberg.net/dorico/) score file (`.dorico`)
  - text file in the  [Guido](https://guido.grame.fr)  language  (`.gmn`)
  - text file in the [Lilypond](https://lilypond.org)  language  (`.lily`)
  - text file in `kern**` [HumDrum](https://www.humdrum.org/rep/kern/) format (`.krn`)
  - quantized [MIDI file](https://www.midi.org/specifications-old/item/standard-midi-files-smf) 
    -  `.mid` export from MuseScore 
    - `.midi` export from Finale
  - PDF file
- `omr/` raw output files obtained with [MuseScore OMR tool](https://musescore.com/import) based on [Audiveris](https://github.com/Audiveris/audiveris).
- `perf/`: recordings of performances of the score, as [MIDI files](https://www.midi.org/specifications-old/item/standard-midi-files-smf) or text file in various serialisation formats.
  The filenames of recordings have the form `VXX_howobtained.format` where howobtained is made of: 
  - the initials of a human player and notes
  - the name of a soft that generated the artificial performance, 
    in particular `HP` if it was made by Finale Human Playback from the ref. score, 
    `HP` is followed by name of preset used.
- `quant/SOFT/` transcription of the MIDI files of `midi/` produced by the software `SOFT` 
  qparse is one of the software options, the others are Finale and MuseScore (MIDI import functionality).
  The format of transcription files can be one of the above formats for reference scores, including quantized MIDI file, or alternatively a pdf of the score or a text representation of qparse parse trees.

See the READMEs for explanations about the contents of the other directories.

The following [table](https://docs.google.com/spreadsheets/d/1inaZqUAFuAXLdlEHa2aJSDRhcuviiGINJciGpf2tUd4) summarizes the current contents of the dataset. Its columns contain:
- the number, name and Composer's name of each extract,
- the existence of a reference score: 
  - `X` in MusicXML,
  - `gmn` in Guido 
- the existence of OMRizations for the extract
  - `MS` means [Audiveris](https://github.com/Audiveris) via online [Musescore service](https://musescore.com/import)
- the existence of human MIDI performances for the extract
- the existence of machine MIDI performances for the extract
- the main features of each extract
  - time signature
  - grace notes, appogiatura etc
  - rests
  - ties or dots
  - 32d notes
  - tuplets (with the tuplet numbers)
  - presence of a pickup bar



