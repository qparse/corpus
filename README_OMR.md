# OMR 

OMR performed with [Audiveris](https://github.com/Audiveris) via Musescore service at:
https://musescore.com/import
OMR raw scores are exported in Music XML and re-edited with Finale 26.

