

# Dataset for MIDI2score transcription 

The dataset contains monophonic extracts from the textbook:

> D’un Rythme à l’autre
> Elisabeth Lamarque, Marie-José Goudard
> ed. Henry Lemoine

The main features of the scores are summarised in the following [table](https://docs.google.com/spreadsheets/d/1inaZqUAFuAXLdlEHa2aJSDRhcuviiGINJciGpf2tUd4). 


Every directory name of the dataset has the form
```
XXX-ComposerName-Piece 
```
where `XXX` is the number of the extract in the method

Every file name in a directory as above has the form
```
XXX_inputMethod.format
```
where `XXX` is the extract number as in the enclosing directory and the pair [`inputMethod`, ` format`]  belongs to one of:
-  [`scan` or `img`, `png` or `pdf`] : original score from the textbook 
-  [`ref` or `manual`, `xml` or `musx` or `mxl`] : MusicXML reference score, produced manually or with OCR and correction  with either Finale, Sibelius or MuseScore (see XML content to know the software).
-  [`ref` or `manual`, `mscz` ] : reference score, produced manually or with OCR and correction  with MuseScore.
- [`ocr`, `mscz`]: score obtained by OCR with MuseScore (raw output).
-  [`ref` or `manual`, `gmn`] : reference score, written manually in Guido.
-  [`ref` or `manual`, `lily`] : reference score, written manually in Lilypond.
-  [`ref`, `krn`] : reference score, in `kern**` HumDrum format.
- [`capital initials of performer`, `mid`]: MIDI file recording of a performance.
- [`finale`, `mid`] : MIDI file quantized  from performance MIDI filerecording with Finale (MIDI import).
- [`musescore`, `mid`] : MIDI file quantized  from performance MIDI file recording with MuseScore (MIDI import).
- [`qparse`, `mid`] : MIDI file quantized  from performance MIDI file recording with qparse.


See the READMEs for explanations about the contents of the other directories.





