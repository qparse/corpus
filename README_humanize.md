# Generating Musical Performances 

Generation of artificial music performance in MIDI, from score files or quantized MIDI files.

How to transform a quantized MIDI file to a MIDI file sounding more like a performance.



**application**: 
building dataset.s for testing transcription procedure
with reference scores + artificial performances (generated from these scores)

---
**Finale Human Playback**

MIDI player with many settings to imitate human style when reading a score.
That includes in particular some timing deviations.
https://usermanuals.finalemusic.com/FinaleMac/Content/Finale/Human_Playback.htm

Possibility to record such playback in a MIDI file?

see also:
- [Garritan & Human Playback Tutorial](https://usermanuals.finalemusic.com/FinaleMac/Content/Finale/GarritanHP.htm)
  Human Playback is a feature of Finale to play a score following many parameters
  Garritan is a virtual instrument lib https://www.garritan.com 
- [Using Finale’s Human Playback in a DAW](https://www.finalemusic.com/blog/using-finales-human-playback-in-digital-audio-workstations/)



---

**Logic pro**

in piano-roll view
Functions > MIDI transform > Humanize

possible to change note position, length, velocity
randomly + following curves

tutos: 
- https://macprovideo.com/article/audio-software/7-ways-to-humanize-beats-and-midi-regions-in-logic-pro-x
- https://youtu.be/KIQ3yvUSK00
  see in https://www.musicsequencing.com/article/humanize-midi
- https://splice.com/blog/humanize-your-drums/



similar functionalities exist in most of the DAWs





---

**Ableton LIve**



- [https://elphnt.io/humanize-your-midi/](https://elphnt.io/humanize-your-midi/) (Ableton Live)
  https://youtu.be/1_p2kSfk5eo (for drum patterns)
  transform timings: 2d half of video
  Packs > Core Library > Swing and Groove 
  = library of presets. e.g. swing effect, random variations



- https://flypaper.soundfly.com/produce/humanize-your-midi-sounds-5-production-tips/





---
**VirtuosoNet**
RNN trained for piano music.

Our research project is developing a system for generating expressive piano perfomrance, or simply 'AI Pianist'. 
The system reads a given music score in MusicXML 
and generates a human-like performance MIDI file.

> VirtuosoNet: A Hierarchical Attention RNN for Generating Expressive Piano Performance from Music Score
> Dasaem Jeong Taegyun Kwon Juhan Nam
> 32nd Conference on Neural Information Processing Systems (NIPS), 2018.
> https://nips2018creativity.github.io/doc/virtuosonet.pdf

> VirtuosoNet: A Hierarchical RNN-based system for modeling expressive piano performance
> Dasaem Jeong, Taegyun Kwon, Yoojin Kim, Kyogu Lee, Juhan Nam. > 20th International Society for Music Information Retrieval Conference (ISMIR), 2019
> http://archives.ismir.net/ismir2019/paper/000112.pdf

GitHub: Python script
https://github.com/jdasam/virtuosoNet



---
**Director Musices**

> Generating Musical Performances with Director Musices
> Anders Friberg, Vittorio Colombo, Lars Frydén and Johan Sundberg
> Computer Music Journal 24(3), pages 23-29, The MIT Press, 2000.
> https://www.jstor.org/stable/3681735

Director musices is a rule system for music performance. 
The point of this system is to take a musical score and make it sound like a real person is playing. 
This is accomplished by applying a set of relatively simple rules.

GitHub: Java code
http://odyssomay.github.io/clj-dm/

Lab:
www.speech.kth.se/music/


Further study exploring the possibilities of using the DM program to produce performances that differ with respect to emotional expression

> Emotional Coloring of Computer-Controlled Music Performances
> Roberto Bresin and Anders Friberg
> Computer Music Journal, 24(4), pages 44–63, The MIT Press, 2000.
> https://kth.diva-portal.org/smash/get/diva2:1246195/FULLTEXT01.pdf

Origin:
DM is a LISP language implementation of the KTH performance rule system for automatic performance of music. It contains about 25 rules that model performers’ renderings of, for example, phrasing, accents, or rhythmic patterns.

> Generative Rules for Music Performance
> Friberg, A.
> Computer Music Journal 15(2), pages 56–71, 1991.

> A Quantitative Rule System for Musical Expression
> Friberg, A. 
> Doctoral dissertation. Stockholm: Royal Institute of Technology, 1995.

---
**Magenta Performance RNN**
Generating Music with Expressive Timing and Dynamics

LSTM-based recurrent neural network designed to model polyphonic music with expressive timing and dynamics. 

https://magenta.tensorflow.org/performance-rnn