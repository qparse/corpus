// penalty model
// model for 3/4 measure
//
// 27/03/2018
// used for 304-Bach

[penalty]

// bar level
0 ( 0 )     0.25
0 ( 1 )     0.10 // blanche
0 ( 2 )     8.85 // grace-note + blanche 
0 ( 3 )    14.05 // 2 grace-notes + blanche
0 ( 1 1 )   8.19 // division habituelle 2/4
0 ( 1 1 1 ) 0.25 // triolet de noires

// beat level
1 ( 0 ) 0.10
1 ( 1 ) 0.05
1 ( 2 ) 5.55
1 ( 3 ) 23.25
1 ( 2 2 )   0.01
1 ( 2 2 2 ) 1.76
1 ( 4 4 4 4 4 ) 3.95
1 ( 4 4 4 4 4 4 4 ) 6.95

// sub-beat = croches
2 ( 0 )   0.07
2 ( 1 )   0.03
2 ( 2 )   9.8
2 ( 3 )   12.3
2 ( 3 3 )   0.01
2 ( 3 3 3 ) 0.03

// sub-sub-beat = doubles
3 ( 0 )   0.05
3 ( 1 )   0.02
3 ( 2 )   5.8
3 ( 3 )   12.3
3 ( 4 4 ) 0.01

4 ( 0 )   0.10
4 ( 1 )   0.01



