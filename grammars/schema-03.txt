// penalty model
// model for 3/4 measure
//
// 29/03/2018
// used for several files (103, 304)


[penalty]

// bar level
0 ( 0 )     1.0
0 ( 1 )     0.50 // blanche
0 ( 2 )     5.85 // grace-note + blanche 
0 ( 3 )     9.05 // 2 grace-notes + blanche
0 ( 1 1 )   8.19 // duolet of quarter
0 ( 1 1 1 ) 0.5  // usual division 3/4 

// beat level
1 ( 0 ) 0.8
1 ( 1 ) 0.4
1 ( 2 ) 5.55
1 ( 3 ) 9.25
1 ( 2 2 )   0.20  // 2 eight notes
1 ( 2 2 2 ) 5.0   // triplet of eight notes
1 ( 4 4 4 4 4 ) 3.95
1 ( 4 4 4 4 4 4 4 ) 6.95

// sub-beat = croches
2 ( 0 )   0.9
2 ( 1 )   0.2
2 ( 2 )   9.8
2 ( 3 )   12.3
2 ( 3 3 )   0.1
2 ( 3 3 3 ) 0.4

// sub-sub-beat = doubles
3 ( 0 )   0.8
3 ( 1 )   0.2
3 ( 2 )   5.8
3 ( 3 )   8.3
3 ( 4 4 ) 0.07

4 ( 0 )   0.9
4 ( 1 )   0.3



