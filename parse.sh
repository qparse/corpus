#!/bin/sh

# first argument : input midi filename. 
FILE=$1 
MIDI="perf/$FILE.mid"
SUFFIX="-PS4"
MEI="qparse/$FILE$SUFFIX.mei"
SVG="qparse/$FILE$SUFFIX.svg"
PDF="qparse/$FILE$SUFFIX.pdf"

# second argument : time signature numerator
TSN=$2

# third argument : time signature denominator
TSD=$3
TS="$TSN/$TSD"
GRAMMAR="../grammars/schema-$TSN$TSD.wta"

# fourth argument : tempo in bpm.
TEMPO=$4

# other options
#CLEF="-clef F4"
CONFIG="../params.ini"


../monoparse -v 5 -a $GRAMMAR -m $MIDI -config $CONFIG -tempo $TEMPO -ts $TS $CLEF -max -o $MEI &&
verovio -o $SVG $MEI &&
inkscape -o $PDF $SVG