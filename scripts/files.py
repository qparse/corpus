#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 12:23:27 2023

@author: jacquema
"""

import os
from pathlib import Path
import re

root = '/Users/jacquema/Datasets/Lamarque-Goudard/'

def opus():
    '''sorted list of all entries in the dataset'''
    res = []
    for d in os.listdir(root):
        if re.search("\A[1-4][0-9][0-9]", d) != None:
            res.append(d)
    res.sort()
    return res

def mv_img():
    for d in opus():
        p = Path(root, d)
        print('mkdir', Path(p, 'img'))
        #os.mkdir(Path(root, d, 'img'))
        for f in os.listdir(p):
            pf = Path(p, f)
            if os.path.isfile(pf) and (pf.suffix in ['.pdf', '.png', '.jpg']):
                print('  mv ', pf, Path(p, 'img', f))
                #os.rename(pf, Path(p, 'img', f))



def is_machine_perf(f):
    if re.search("\A[1-4][0-9][0-9][-_]HP", f) != None: 
        return True
    elif re.search("\A[1-4][0-9][0-9][-_]bandlab", f) != None: 
        return True
    else:
        return False
       
        
def mv_perf():
    for d in opus():
        p = Path(root, d, 'perf')
        if p.exists() and p.is_dir():
            for f in os.listdir(p):
                pf = Path(p, f)
                if os.path.isfile(pf) and (pf.suffix in ['.midi', '.mid']):
                    if is_machine_perf(f):
                        mv_perf1(f, p, 'machine')
                    else:
                        mv_perf1(f, p, 'human')                    
                    

def mv_perf1(f, p, subdir):
    pm = Path(p, subdir)
    if not pm.exists():
        print('mkdir', pm)
        os.mkdir(pm)
    print('  mv ', Path(p, f), Path(pm, f))
    os.rename(Path(p, f), Path(pm, f))  

def search_hperf():
    for d in opus():
        p = Path(root, d, 'perf', 'human')
        if p.exists() and p.is_dir():
            print(p)
            
            
    
    